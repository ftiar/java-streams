import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Arrays;
import java.util.Collections;

public class Main {
	public static void main(String[] args) throws IOException
	{
		/**
		 * First exercise : stream & hashmap
		 */
		String fileName = "/home/webmasterflo/eclipse-workspace/rattrapage_java/src/films.txt";
		
	    Map<String, List<String>> actors_list = new HashMap<String, List<String>>();
	    		
		//read file into stream, try-with-resources
		Stream<String> stream = Files.lines(Paths.get(fileName));
		
		// Populate the Map<String, List<String>> object
		stream.map(ligne -> Arrays.asList(ligne.split(";"))).forEach(split -> actors_list.put(split.get(0), split.subList(1, split.size())));

		// Display the map with actors
		System.out.println(Arrays.asList(actors_list));

		/**
		 * Second exercise : On cherche maintenant à afficher le nombre total d'acteurs ayant joué dans au moins un film. 
		 * Flatmap permet de mettre à plat un stream, en passant par exemple d'une liste de String à une String tout court
		 */
		Stream<String> actors_stream = Files.lines(Paths.get(fileName));
		actors_stream.flatMap(acteur -> Stream.of(acteur.split(";")).skip(1))
						.limit(50)
						.forEach(System.out::println);

		/**
		 * Third exercise : Au lieu d'afficher les 50 premiers, comptez le nombre d'acteurs et affichez le résultat. 
		 */
		Stream<String> actors_stream_2 = Files.lines(Paths.get(fileName));
		long count = actors_stream_2.flatMap(acteur -> Stream.of(acteur.split(";")).skip(1))
									.count();
		
		System.out.println( count );
		
		/**
		 * 4th exercise : Dans un premier temps, nous allons éviter les doublons en stockant les acteurs dans une structure de données qui a la propriété de ne pas enregistrer les doublons. 
		 * Il s'agit de l'interface Set
		 * Nous utilisons l'implémentation HashSet
		 */
		HashSet actors = new HashSet();
		Stream<String> actors_stream_3 = Files.lines(Paths.get(fileName));
		actors_stream_3.flatMap(acteur -> Stream.of(acteur.split(";")).skip(1))
						.forEach(actor -> actors.add( actor ) );

		// On constate donc qu'on passe de 1371 acteurs à 1329 en retirant les doublons
		System.out.println(actors.size());
		
		/**
		 * 5th exercise : En fait, il existe une méthode Stream.distinct(). Comment peut-on l'utiliser pour trouver le nombre total d'acteurs. 
		 */
		Stream<String> actors_stream_4 = Files.lines(Paths.get(fileName));
		long count2 = actors_stream_4.flatMap(acteur -> Stream.of(acteur.split(";")).skip(1))
									.distinct() // Comme ceci
									.count();
		
		System.out.println( count2 ); // Retourne 1329 également


		/**
		 * 6th exercise : On souhaite maintenant calculer le nombre de films pour chaque acteur.
		 */
		Stream<String> stream_6 = Files.lines(Paths.get(fileName));

        Map<String, Long> result =
        		stream_6.flatMap(acteur -> Stream.of(acteur.split(";")).skip(1))
				.collect(
                        Collectors.groupingBy(
                        		s->s, Collectors.counting()
                        )
				 );

        // Affichage du nombre de films joués par chaque acteur
        System.out.println(result);
        
        // Affichage du nombre de films joué par Brad Pitt :
        System.out.println(result.get("Brad Pitt")); // 26


		/**
		 * 7th exercise : Il existe une méthode Function.identity(). Comment peut-on l'utiliser dans notre cas ?
		 * Cette fonction permet de gagner un peu en performance (soulage la mémoire)
		 */
		Stream<String> stream_7 = Files.lines(Paths.get(fileName));

        Map<String, Long> get_number_of_films_by_actor =
        		stream_7.flatMap(acteur -> Stream.of(acteur.split(";")).skip(1))
				.collect(
                        Collectors.groupingBy(
                        		Function.identity(), Collectors.counting()
                        )
				 );
        
		/**
		 * 8th exercise : On aimerait ne pas avoir une liste d'acteurs comme valeur de la Map résultante mais plutôt directement le nombre de fois qu'un acteur apparaît dans la liste.
		 */

        Map<String, Long> finalMap = new LinkedHashMap<>();

        // On trie la liste des acteurs dans l'ordre de celui qui a joué dans le plus de films
        get_number_of_films_by_actor.entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue()
                        .reversed()).forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));

        System.out.println(finalMap);

        
	}
}
